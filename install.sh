#!/usr/bin/env bash

###
# Install oh my zsh
###

printf "\n🚀 Installing oh-my-zsh\n"
if [ -d "${HOME}/.oh-my-zsh" ]; then
  printf "oh-my-zsh is already installed\n"
else
  sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
fi

# Include environment
. ./profile/zprofile

###
# Installing dotfiles
###

# Install to zsh profile
ln -fs ${DOTFILES_DIR}/profile/zprofile ${HOME}/.zprofile
ln -fs ${DOTFILES_DIR}/profile/zshrc ${HOME}/.zshrc

if is-wsl; then
  # Create dir for symlinks to Windows executables
  #   E.g.: ssh > ssh.exe
  mkdir -p ~/.local/bin
  ln -fs "/mnt/c/Users/kuurstra/AppData/Local/Microsoft/WinGet/Packages/AgileBits.1Password.CLI_Microsoft.Winget.Source_8wekyb3d8bbwe/op.exe" ~/.local/bin/op
  # Make dotfiles available from WSL home dir
  ln -fs "${DOTFILES_DIR}" "${HOME}/.dotfiles"
fi

# Install ssh config
. ./install/ssh.sh

# Install git config
. ./install/git.sh
