if type "ksm" >/dev/null; then
    function kpsw() {
        if [[ $# -lt 1 ]]; then
            echo "Usage: kpswd <service>"
            return 1
        fi

        local service=$1

        # Lookup uid
        if uid=$(ksm secret get -t "$service" -q "$.uid" --raw 2>/dev/null) && [[ "$uid" == "[]" ]]; then
            if uid=$(ksm secret get -t "$service" -q "$.[0].uid" --raw 2>/dev/null) && [[ "$uid" == "[]" ]]; then
                echo "error: could not obtain record uid for $service"
                return 1
            else
                echo "warning: Found multiple records for $service, using first one"
            fi
        fi

        local username
        # Copy the username to the clipboard
        if ! username=$(ksm secret get "$uid" -f login 2>/dev/null); then
            echo "error: could not obtain username for $service"
            return 1
        fi

        echo -n "$username" | clipcopy
        echo "✔ username for service $service copied to the clipboard. Press Enter to continue"
        read

        local password
        # Copy the password to the clipboard
        if ! password=$(ksm secret get "$uid" -f password 2>/dev/null); then
            echo "error: could not obtain password for $service"
            return 1
        fi

        echo -n "$password" | clipcopy
        echo "✔ password for $service copied to clipboard. Press Enter to continue"
        read

        # If there's a one time password, copy it to the clipboard
        local totp
        if totp=$(ksm secret tot "$uid" 2>/dev/null) && [[ -n "$totp" ]]; then
            echo -n "$totp" | clipcopy
            echo "✔ TOTP for $service copied to clipboard"
        fi

        (sleep 20 && clipcopy </dev/null 2>/dev/null) &!
    }
fi
