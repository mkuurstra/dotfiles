if (
  type pipenv >/dev/null 2>&1 \
  || type /opt/homebrew/bin/pipenv >/dev/null 2>&1 \
 ) ; then
  export PIPENV_VENV_IN_PROJECT=1
fi
