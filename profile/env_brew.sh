# Determine if brew exists
if (
  type brew >/dev/null 2>&1 \
  || type /opt/homebrew/bin/brew >/dev/null 2>&1 \
  || type /home/linuxbrew/.linuxbrew/bin/brew >/dev/null 2>&1
 ) ; then

  # Determine how to call brew
  if type brew >/dev/null 2>&1; then
    local brew=brew
  elif type /opt/homebrew/bin/brew >/dev/null 2>&1; then
    local brew=/opt/homebrew/bin/brew
  elif type /home/linuxbrew/.linuxbrew/bin/brew >/dev/null 2>&1; then
    local brew=/home/linuxbrew/.linuxbrew/bin/brew
  fi

  # Set environment variables using brew command
  eval "$($brew shellenv)"
  FPATH="$($brew --prefix)/share/zsh/site-functions:${FPATH}"
fi
