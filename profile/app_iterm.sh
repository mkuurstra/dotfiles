if [[ -e "${HOME}/.iterm2_shell_integration.zsh" ]]; then
  source "${HOME}/.iterm2_shell_integration.zsh"
  alias tab_red="iterm2_tab_color 255 0 0"
  alias tab_green="iterm2_tab_color 0 255 0"
  alias tab_blue="iterm2_tab_color 0 0 255"
fi
