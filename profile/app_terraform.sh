if type "terraform" > /dev/null; then
    complete -o nospace -C $(brew --prefix)/bin/terraform terraform
fi
