if [[ -f "/usr/bin/git" ]]; then
  alias git_pull_subdirs="find . -type d -name .git -exec sh -c 'cd {}/.. && echo \$PWD && current_branch=\$(git rev-parse --abbrev-ref HEAD) && git checkout main && git pull origin main && git checkout \$current_branch' \;"
fi
