# Install git config file

# Create new .gitconfig file and include gitconfig from dotfiles
cat > ${HOME}/.gitconfig  <<EOL
# Git config file
[include]
  path =  ${DOTFILES_DIR}/apps/git/gitconfig
EOL

# Include config based on OS
if is-wsl; then
  cat >>${HOME}/.gitconfig  <<EOL
# Include config for WSL
[include]
  path = ${DOTFILES_DIR}/apps/git/gitconfig.wsl
EOL
  cat >${DOTFILES_DIR}/../.gitconfig  <<EOL
[include]
  path =  ~/.dotfiles/apps/git/gitconfig
EOL
elif is-macos; then
  cat >>${HOME}/.gitconfig  <<EOL
# Include config for MacOS
[include]
  path = ${DOTFILES_DIR}/apps/git/gitconfig.macos
EOL
else
  echo "Unknown OS, skipping OS specific gitconfig"
  echo "Please add your OS specific gitconfig to ${HOME}/.gitconfig.local"
  echo "Example:"
  echo "
# Include config for Linux
[include]
  path = ${DOTFILES_DIR}/apps/git/gitconfig.remote
"
fi

# Create gitconfig file for overrides, this is used in dotfiles gitconfig
if [ ! -f ${HOME}/.gitconfig.local ]; then
  touch ${HOME}/.gitconfig.local
fi
