# Install SSH config file

cat > ${HOME}/.ssh/config <<EOL
# SSH config file
Include ~/.dotfiles/apps/ssh/config
EOL

if is-wsl; then
  # On WSL create SSH config in WIN home dir
  cat > ${DOTFILES_DIR}/../.ssh/config  <<EOL
# SSH config file
Include ~/.dotfiles/apps/ssh/config
EOL
  # On WSL use Windows SSH by default
  ln -fs "/mnt/c/Windows/System32/OpenSSH//ssh.exe" ~/.local/bin/ssh
  ln -fs "/mnt/c/Windows/System32/OpenSSH//scp.exe" ~/.local/bin/scp
fi
