# My dotfiles

## Installation

- For WSL, go to Windows homedir
- Everything else use homedir: `cd ~`

```shell
git clone https://gitlab.com/mkuurstra-works/dotfiles.git .dotfiles
cd .dotfiles && ./install.sh
```

## WSL

To get permissions working on WSL add this to `/etc/wsl.conf`:

```toml
[automount] 
options = "metadata" 
```

### WSL slow prompt

```shell
git config --add oh-my-zsh.hide-dirty 1
git config --add oh-my-zsh.hide-status 1
git config --add oh-my-zsh.hide-info 1
```

### Windows Terminal Powerline Fonts

- <https://github.com/microsoft/cascadia-code/wiki/Installing-Cascadia-Code>
- Or install from here: [CascadiaCodePL.ttf](files/CascadiaCodePL.ttf)
