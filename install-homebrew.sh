#!/usr/bin/env bash

# Global vars
SOURCE_DIR="./Brewfile"
TMP_BREWFILE="/tmp/Brewfile"

cleanup_build() {
  # Cleanup build files

  rm -f "${TMP_BREWFILE}"
}

build_brewfile() {
  # Join Brewfile materials in single file in the right order

  local destination_file=${1}

  cat ${SOURCE_DIR}/*.shared > ${destination_file}
  if is-macos; then
    # Extract materials for MacOS
    cat ${SOURCE_DIR}/*.mac >> ${destination_file}
  # elif is-wsl; then
  #   # Extract materials for WSL
  #   cat ${SOURCE_DIR}/*.wsl >> ${destination_file}
  fi
}

install_brewfile() {
  # Install specified Brewfile

  local brewfile=${1}

  brew bundle install --file="${brewfile}" --cleanup
}

##
## Start
##

# Install Homebrew if it does not exist
if ! type brew >/dev/null 2>&1; then
  /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
fi

# Build Brewfile and install
build_brewfile "${TMP_BREWFILE}"
install_brewfile "${TMP_BREWFILE}"

# Mac things

if is-macos; then
  # Fix some of the quick look plugins
  xattr -r -d com.apple.quarantine /Applications/Quick\ Look/*
fi

# # Cleanup generated build files
# cleanup_build
