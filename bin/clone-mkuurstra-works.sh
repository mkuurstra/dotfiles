glab auth login --token $GITLAB_COM_PERSONAL_ACCESS_TOKEN
mkdir -p ~/code/gitlab
cd ~/code/gitlab && glab repo clone \
  --archived=false \
  --preserve-namespace \
  --paginate \
  --group mkuurstra-works
