#!/bin/bash

# Controleer of een domeinnaam is opgegeven
if [ -z "$1" ]; then
  echo "Gebruik: $0 <domeinnaam>"
  exit 1
fi

DOMAIN=$1
PORT=443

# Maak verbinding met de server via OpenSSL en verkrijg het certificaat
END_DATE=$(echo | openssl s_client -servername "$DOMAIN" -connect "$DOMAIN:$PORT" 2>/dev/null |
  openssl x509 -noout -enddate | cut -d= -f2)

# Controleer of er een einddatum is verkregen
if [ -z "$END_DATE" ]; then
  echo "Kon de einddatum van het certificaat niet ophalen voor $DOMAIN"
  exit 1
fi

# Toon de einddatum
echo "Certificaat verloopt op: $END_DATE"
