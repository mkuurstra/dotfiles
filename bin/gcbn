#! /usr/bin/env bash

declare DATE=$(date +%Y%m%d) # Use date as prefix in branch name
declare DESC=""              # Use user description for postfix in branch name
declare PROMPT_CMD="read"    # Util for prompting in bash

# Function to check if a branch exists
branch_exists() {
  git show-ref --quiet refs/heads/$1
}

# Check for "main" first (preferred default)
if branch_exists main; then
  git checkout main
  echo "Checked out branch 'main'"
# If "main" doesn't exist, try "master"
elif branch_exists master; then
  git checkout master
  echo "Checked out branch 'master'"
# Neither branch exists, so provide an error
else
  echo "ERROR: Neither 'main' nor 'master' branch found in this repository."
  exit 1
fi

git pull

echo "" # New line for readability

# If arguments are provided, join them with hyphens to form the description
if [[ $# -gt 0 ]]; then
  DESC=$(echo "$@" | tr ' ' '-')
fi

# If no description, prompt the user for one
if [[ -z "$DESC" ]]; then
  $PROMPT_CMD -p "Enter branch description (or press Enter to skip): " DESC
  DESC=$(echo "$DESC" | tr ' ' '-')
fi

# Construct the branch name
declare BRANCH_NAME="${DATE}-${DESC}"
BRANCH_NAME=$(echo "$BRANCH_NAME" | tr '[:upper:]' '[:lower:]')

# If a description was provided (or Enter was pressed), create the branch
declare CONFIRM="" # Used to register user confirmation
if [[ -n "$DESC" ]]; then
  git checkout -b "$BRANCH_NAME"
  exit 0
else
  # If no description and Enter was pressed, create a branch with just the date by default
  $PROMPT_CMD -p "Create branch '$DATE'? (y/n): " CONFIRM
  echo ""
fi

if [[ "$CONFIRM" =~ ^[Nn]$ ]]; then # Check for explicit 'n' or 'N'
  echo "Branch creation cancelled."
  exit 1
elif [[ ! "$CONFIRM" =~ ^[Yy]?$ ]]; then # Check for gibberish or invalid input (not Y, y, or empty)
  echo "Invalid input. Branch creation cancelled."
  exit 1
else
  git checkout -b "$DATE"
fi
