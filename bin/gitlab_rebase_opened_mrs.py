#!/usr/bin/env pipenv-shebang

# pipx install pipenv-shebang

import argparse
import sys

import requests
import urllib3


def main():
    # Parse command-line arguments
    parser = argparse.ArgumentParser(
        description="Rebase all open merge requests for a GitLab project."
    )
    parser.add_argument(
        "--gitlab_base_url",
        help="The base URL of the GitLab API.",
        default="https://gitlab.com",
    )
    parser.add_argument(
        "--token", required=True, help="Your GitLab personal access token."
    )
    parser.add_argument(
        "--project_id", required=True, help="The ID of the GitLab project."
    )
    parser.add_argument(
        "--insecure",
        action="store_true",
        help="Ignore SSL certificate verification (useful for self-signed certificates).",
    )

    args = parser.parse_args()

    gitlab_base_url = args.gitlab_base_url.rstrip("/")
    gitlab_token = args.token
    project_id = args.project_id
    verify_ssl = not args.insecure  # Invert flag for requests.verify

    # Suppress SSL warnings if --insecure is set
    if args.insecure:
        urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

    # Headers for GitLab API
    headers = {"Authorization": f"Bearer {gitlab_token}"}

    # Get opened merge requests
    try:
        response = requests.get(
            f"{gitlab_base_url}/api/v4/projects/{project_id}/merge_requests",
            headers=headers,
            params={"state": "opened"},
            verify=verify_ssl,
        )

        if response.status_code != 200:
            print(
                f"Failed to fetch merge requests: {response.status_code}, {response.text}"
            )
            sys.exit(1)

        # Parse the response JSON
        merge_requests = response.json()

        if not merge_requests:
            print("No open merge requests found.")
            return

        # Rebase each merge request
        for mr in merge_requests:
            iid = mr["iid"]
            rebase_url = f"{gitlab_base_url}/api/v4/projects/{project_id}/merge_requests/{iid}/rebase"
            rebase_response = requests.put(
                rebase_url, headers=headers, verify=verify_ssl
            )

            if rebase_response.status_code == 200:
                print(f"Successfully triggered rebase for MR IID: {iid}")
            else:
                print(
                    f"Failed to rebase MR IID {iid}: {rebase_response.status_code}, {rebase_response.text}"
                )
    except Exception as e:
        print(f"An error occurred: {str(e)}")
        sys.exit(1)


if __name__ == "__main__":
    main()
